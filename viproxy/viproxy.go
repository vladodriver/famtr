package viproxy

import (
	"fmt"
	"io"
	"net/http"
	"strings"
)

//Package viproxy proxies /ddddd (vid) path to https://vimeo.com/ddddd using optionaly referer

//VProxy interface implements http.Handler and modificators for
// request and response
type VProxy struct {
	referer string
	cl      ViClient
}

//ViClient interface
type ViClient interface {
	Do(*http.Request) (*http.Response, error)
}

//ViURL constant base Vimeo url path
const ViURL = "https://player.vimeo.com/video/"

//DefaultViClient impl.
var DefaultViClient = http.DefaultClient

//NewVProxy construct new Prody
func NewVProxy(referer string, cl ViClient) *VProxy {
	if cl == nil {
		cl = DefaultViClient
	}
	return &VProxy{referer, cl}
}

func (prx *VProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var req *http.Request
	var res *http.Response

	vid := strings.TrimPrefix(r.URL.Path, "/")
	rqurl := ViURL + vid

	//ViURL is valid form, error from NewRequest impossible
	req, _ = http.NewRequest("GET", rqurl, nil)

	req.Header.Set("Referer", prx.referer)

	if res, err = prx.cl.Do(req); err != nil {
		w.WriteHeader(500)
		fmt.Fprintf(w, "Get request to: %s failed with error: %v!", rqurl, err)
	} else {
		defer res.Body.Close()
		w.Header().Set("referer", prx.referer)
		io.Copy(w, res.Body)
	}
}
