package viproxy

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestNewViProxy(t *testing.T) {
	type args struct {
		referer string
		cl      ViClient
	}
	tests := []struct {
		name string
		args args
		want *VProxy
	}{
		{
			name: "Http clinet nil must set to default http",
			args: args{"http://cokoliv.cz", nil},
			want: &VProxy{"http://cokoliv.cz", DefaultViClient},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewVProxy(tt.args.referer, tt.args.cl); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewViProxy() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVProxy_ServeHTTP(t *testing.T) {

	type input struct {
		vipr *VProxy
		req  *http.Request
		w    *httptest.ResponseRecorder
		cl   ViClient
	}

	type result struct {
		code int
		body string
	}

	//response result
	tests := []struct {
		name   string
		args   input
		result result
	}{
		{
			name: "Failed client must return code 500 and error message",
			args: input{
				vipr: NewVProxy("refer.err", NewClientMock(true, fmt.Errorf("Client failed"))),
				req:  httptest.NewRequest("GET", "https://chcipl.pes", nil),
				w:    httptest.NewRecorder(),
			},
			result: result{500, "Get request to: https://player.vimeo.com/video/ failed with error: Client failed!"},
		},

		{
			name: "Goor client response ",
			args: input{
				vipr: NewVProxy("refer.to", NewClientMock(false, nil)),
				req:  httptest.NewRequest("GET", "https://toje.ok", nil),
				w:    httptest.NewRecorder(),
			},
			result: result{200, "Good response"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//run handler using testing input args
			handler := http.HandlerFunc(tt.args.vipr.ServeHTTP)
			handler.ServeHTTP(tt.args.w, tt.args.req)
			body, _ := ioutil.ReadAll(tt.args.w.Body)
			got := result{tt.args.w.Code, string(body)}
			want := tt.result

			if !reflect.DeepEqual(got, want) {
				t.Errorf("Got: %v but want %v", got, want)
			}
		})
	}

}

func TestRefererInceptions(t *testing.T) {
	t.Run("Server response and client sub-request must have referer header set to *VProxy.referer parameter", func(t *testing.T) {
		referer := "test.referer.to"
		clmock := NewClientMock(false, nil)
		vipr := NewVProxy(referer, clmock)
		ts := httptest.NewServer(vipr)
		defer ts.Close()
		res, _ := http.Get(ts.URL)
		got := res.Header.Get("referer")
		want := referer
		if got != want {
			t.Errorf("Got referer server: '%v', but want referer: '%v'", got, want)
		}
	})
}

type ClientMock struct {
	failed bool
	err    error
}

func NewClientMock(failed bool, err error) *ClientMock {
	return &ClientMock{failed, err}
}

func (cm *ClientMock) Do(req *http.Request) (res *http.Response, err error) {
	if cm.failed {
		return nil, cm.err
	}

	//create fake response using httptest.ResponseRecorder
	fakeServerResponse := func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Good response")
	}
	clreq := httptest.NewRequest("GET", "https://only.test", nil)
	w := httptest.NewRecorder()
	fakeServerResponse(w, clreq)
	res = w.Result()
	return res, nil
}
