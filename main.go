package main

import (
	"embed"
	"famtr/viproxy"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const etag = "Spav1"

// content is our static web server content.
//
//go:embed static
//go:embed data.json
//go:embed favicon.ico
var content embed.FS

// Spa is middleware for SPA
type spa struct {
	mux *http.ServeMux
	vp  *viproxy.VProxy
	dh  *DataHandler
}

func (s *spa) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	trp := strings.Trim(req.URL.Path, "/")
	spl := strings.Split(trp, "/")
	log.Printf("%s->prom: %s -> %s", req.Method, req.RemoteAddr, req.URL)
	if len(spl) > 0 {
		f := spl[0]
		if f == "video" {
			http.StripPrefix("/video/", s.vp).ServeHTTP(w, req)
		} else if f == "data" {
			http.StripPrefix("/data/", s.dh).ServeHTTP(w, req)
		} else if f == "static" {
			cacheableStatic(w, req)
		} else {
			if f == "favicon.ico" {
				spaHandler("favicon.ico", w, req)
			} else { //SPA - all other /<anything>/... resolve as index.html
				spaHandler("static/index.html", w, req)
			}
		}
	} else {
		spaHandler("static/index.html", w, req)
	}
}

// nake http.FileServer cacheable
func cacheableStatic(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Cache-Control", "max-age=31536000, public, immutable")
	http.FileServer(http.FS(content)).ServeHTTP(w, req)
}

// DataHandler secure
type DataHandler struct {
	key string
}

// NewDataHandler security
func NewDataHandler() *DataHandler {
	return &DataHandler{genAPIKey()}
}

func genAPIKey() string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyz0123456789")
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, 32)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	key := string(b)
	fmt.Printf(`
**************Atention****************************
One-off Api-key: "%s"
- only use once, now vil be regenerated
**************************************************
`, key)
	return key
}

func (dh *DataHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	key := strings.TrimPrefix(req.URL.Path, "/")
	kl := len(key)
	if kl != 32 {
		w.WriteHeader(400)
		fmt.Fprintf(w,
			"Ivalid key length: %d, must be 32, key: %s is invalid!", kl, key)
	} else if key != dh.key {
		w.WriteHeader(401)
		fmt.Fprintf(w,
			"Access denied - bad authentication! KEY: '%s' is invalid!", key)
	} else {
		spaHandler("data.json", w, req)
		dh.key = genAPIKey()
	}
}

func spaHandler(path string, w http.ResponseWriter, req *http.Request) {
	file, err := content.Open(path)
	if err != nil {
		http.NotFoundHandler().ServeHTTP(w, req)
		return
	}

	stat, _ := file.Stat()
	data, _ := io.ReadAll(file)
	size := strconv.Itoa(int(stat.Size()))
	w.Header().Del("Date")
	w.Header().Add("Content-Length", size)
	w.Header().Add("ETag", etag)

	if ifn := req.Header.Get("If-None-Match"); ifn == etag {
		w.WriteHeader(304)
		return
	}
	w.Write(data)
}

// StartServer run application for ember site URL - referer mux and listen on port
// If fail return error
func StartServer(referer string, mux *http.ServeMux, address string) (err error) {
	spa := &spa{
		mux,
		viproxy.NewVProxy(referer, &http.Client{Timeout: 30 * time.Second}),
		NewDataHandler(),
	}

	fmt.Printf("Referer proxy for: %s started at: http://%s\n", referer, address)
	return http.ListenAndServe(address, spa)
}

func main() {
	rf := "https://mathematicator.com"
	var addr string
	if len(os.Args) >= 2 {
		addr = os.Args[1]

	} else {
		addr = "localhost:8080"
	}
	if err := StartServer(rf, http.NewServeMux(), addr); err != nil {
		fmt.Printf("Run video proxy server failed with error: %s\n", err)
	}
}
