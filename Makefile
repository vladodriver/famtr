exe = famtr
static = rm -rf static && cp -r devel static
minify = $(static) && minify --html-keep-document-tags -r static -o .
gob = $(minify) && go build -x -ldflags "-s -w"
gb = $(minify) && go build -x -ldflags "-s -w" && $(upx)
run = rm -rf static && cp -rfv devel static && go run .

upx = upx --lzma $(exe)
kill = killall -q -s 9 $(exe)

build:
	$(gob)
bupx:
	$(gob) && $(upx)
upx:
	$(upx)
run:
	$(run)

start:
	$(upx)
	$(kil)
	./$(exe)

clean:
	rm $(exe)
