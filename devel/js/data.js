class DataReady extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.readyData()
  }

  readyData(key) {
    let courses = localStorage.getItem('famtr')
    if (courses === null) {
      this.createKeyForm()
      this.setAttribute('state', 'auth')
      const okb = this.querySelector('#okbtn')
      okb.onclick = (e) => { this.onGrabKey(e) }
    } else {
      this.activeMenu()
    }
  }

  activeMenu() {
    const menu = document.createElement('famtr-menu')
    menu.id = 'crs_menu'
    document.body.appendChild(menu)
  }

  //Grab user input auth secret key TODO
  createKeyForm() {
    this.innerHTML =
      `<div id="auth_form">
        <fieldset>
          <p class="err">Je třeba vložit API klíč pro načtení vašich dat:</p>
          <input size="32" type="text" id="kinput" placeholder="Vložit tajný klíč zde!!">
          <input type="button" value=" Vložit " id="okbtn">
        </fieldset>
        <div id="message"></div>
      </div>`
  }

  //escape HTML - mitigate client-side xss
  esc(str) {
    return String(str).replace(/[^\w. ]/gi, c => {
      return '&#' + c.charCodeAt(0) + ';';
    });
  }

  onGrabKey(e) {
    const msgel = this.querySelector('#message')
    let ki = this.querySelector('#kinput')
    this.addMsg(`Vložen klíč: <b>${this.esc(ki.value)}</b>`, 'ok')
    this.fetchDataMenu(ki.value)
  }

  addMsg(msg, msgtype) {
    const msgel = this.querySelector('#message')
    const h = `<p class="${msgtype}">${msg}</p>`
    msgel.innerHTML = msgel.innerHTML + h
  }

  clear() {
    this.innerHTML = ''
    this.setAttribute('state', 'none')
  }

  setStorage(data) {
    localStorage.setItem('famtr', JSON.stringify(data))
    localStorage.setItem('famtr_folding', '[]')
  }

  clearStorage() {
    localStorage.removeItem('famtr')
    localStorage.removeItem('famtr_folding')
  }

  fetchDataMenu(key) {
    fetch('/data/' + encodeURIComponent(this.esc(key))).then(res => {
      if (res.ok) {
        res.json().then(data => {
          this.addMsg(`Data downolad and parsed OK, saving to local storage.`, 'ok')
          this.setStorage(data)
          this.clear()
          this.activeMenu()
        }).catch(e => {
          this.clearStorage()
          this.addMsg(`Data downoladed  OK, but not parsed to JSON error: ${e}`, 'err')
        })
      } else {
        this.clearStorage()
        this.addMsg(`Špatný klíč: <b>${res.status}: ${res.statusText}</b>`, 'err')
        res.text().then(data => this.addMsg(
          `Odpověď serveru: <b>${data}</b>`, 'err'))
      }
    })
  }
}

customElements.define('data-ready', DataReady);