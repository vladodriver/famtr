class FamtrMenu extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.renderData()
  }

  renderData() {
    const courses = localStorage.getItem('famtr')
    const json_course_list = JSON.parse(courses)
    this.innerHTML = this.makeCourses(json_course_list.links)
    this.initFolding()
  }

  setFoldEl(idx, fold) {
    let status = this.getUnfoldStatus();
    if (fold) {
      if (status.indexOf(idx) > -1) {
        status.splice(status.indexOf(idx), 1) //remove from unfolded list
      }
    } else {
      if (status.indexOf(idx) == -1) {
        status.push(idx) //add from unfolded list
      }
    }
    localStorage.setItem('famtr_folding', JSON.stringify(status))
  }

  getUnfoldStatus() {
    try {
      return JSON.parse(localStorage.getItem('famtr_folding'))
    } catch {
      return null
    }
  }

  getFoldClickEvh(nidx) {
    return (e) => {
      if (e.target.dataset.folded == "false") {
        console.log("Mark as folded", nidx)
        this.setFoldEl(nidx, true)
        e.target.dataset.folded = "true"
        e.target.querySelector("ul").dataset.hiden = "true"
      } else if (e.target.dataset.folded == "true") {
        console.log("Mark unfolded", nidx)
        this.setFoldEl(nidx, false)
        e.target.dataset.folded = "false"
        e.target.querySelector("ul").dataset.hiden = "false"
      }
    }
  }

  initFolding() {
    const fld = document.querySelectorAll('[data-folded]')
    let unfold_status = this.getUnfoldStatus()
    console.log("Get initial unfolding status:", unfold_status)

    //bad data or deleted data
    if (unfold_status === null || !Array.isArray(unfold_status)) {
      localStorage.setItem('famtr_folding', '[]')
      unfold_status = this.getUnfoldStatus() //fix
      console.log("Fix initial unfolding status:", unfold_status)
    }

    fld.forEach((e, nidx) => {
      e.onclick = this.getFoldClickEvh(nidx)
      if (unfold_status.indexOf(nidx) > -1) {
        e.dataset.folded = "false"
        e.querySelector("ul").dataset.hiden = "false"
      }
    })
  }

  makeCourses(courses) {
    var html = `<nav><ul>`
    for (var c in courses) {
      html += `<li data-folded="true">${courses[c].a}
      ${this.makeChapters(courses[c].links)}</li>`
    }
    html += `</ul></nav>`
    return html
  }

  makeChapters(cse) {
    var html = `<ul data-hiden="true">`
    for (var c in cse) {
      html += `<li><a href="${this.getVid(cse[c])}">${cse[c].a}</a></li>`
    }
    html += `</ul>`
    return html
  }

  getVid(chapter) {
    return new URL(chapter.links[0]).pathname
  }
}

customElements.define('famtr-menu', FamtrMenu);